package com.qsl.circle.prototype;

/**
 * @author yunlongn
 * @date 2021/2/1 20:10
 */
public class Loop {

    private Circle circle;

    public Circle getCircle() {
        return circle;
    }

    public void setCircle(Circle circle) {
        this.circle = circle;
    }
}
