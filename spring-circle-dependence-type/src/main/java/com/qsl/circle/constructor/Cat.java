package com.qsl.circle.constructor;

/**
 * @author yunlongn
 * @date 2021/3/10 22:32
 */
public class Cat {

    private Dog dog;

    public Cat(Dog dog) {
        this.dog = dog;
    }

    public Dog getDog() {
        return dog;
    }

    public void setDog(Dog dog) {
        this.dog = dog;
    }
}
