package com.qsl.test;

import com.qsl.SimpleBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author yunlongn
 * @date 2021/2/3 22:10
 */
public class SimpleTest {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("no-dependence.xml");
        SimpleBean simpleBean = (SimpleBean)context.getBean("simpleBean");
        System.out.println(simpleBean.getClass().getTypeName());
        simpleBean.sayHello("yunlongn");
    }
}
