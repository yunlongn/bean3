package com.qsl;

/**
 * @author yunlongn
 * @date 2021/2/3 22:10
 */
public class SimpleBean {

    public void sayHello(String name) {
        System.out.println("hello, " + name);
    }
}
