package com.qsl;

/**
 * @author yunlongn
 * @date 2021/2/1 22:13
 */
public class SimpleAspect {

    public void before() {
        System.out.println("前置增强处理...");
    }
}
