package com.qsl.circle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author yunlongn
 * @date 2021/2/1 20:10
 */
@Component
public class Circle {

    @Autowired
    private Loop loop;

    public void sayHello(String name) {
        System.out.println("hello, " + name);
    }
}
