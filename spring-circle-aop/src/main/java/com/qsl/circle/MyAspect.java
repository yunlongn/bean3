package com.qsl.circle;

/**
 * @author yunlongn
 * @date 2021/2/1 20:13
 */
public class MyAspect {

    public void before() {
        System.out.println("前置增强处理...");
    }
}
